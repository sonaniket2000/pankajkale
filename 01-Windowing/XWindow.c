//Standard Header File
#include <stdio.h> // for printf
#include <stdlib.h> // for exit
#include <memory.h> //memset


//x11 header files

#include <X11/Xlib.h> //forall x window API
#include <X11/Xutil.h>//for x visual info and related API
#include <X11/XKBlib.h>

//macros
#define WIN_WIDTH 800
#define WIN_HEIGHT 600


//global variable declaration
Display *display = NULL;
Colormap colormap;
Window window;

//main
int main(void)
{
	//local function declarations
	void uninitialise(void);


	//local variable declarations
	int defaultScreen;
	int defaultDepth;
	XVisualInfo visualInfo;
	Status status;
	XSetWindowAttributes windowAttributes;
	int styleMask;
	Atom windowManagerDelete;
	XEvent event;
	KeySym keySym;
	//code
	//Step 1.open connection with server 
	display = XOpenDisplay(NULL); //1 api
	if (display == NULL)
	{
		printf("XOpenDisplay() Failed\n");
		uninitialise();
		exit(1); //failure exit 

	}

	// Step 2.get default screen from above display
	defaultScreen = XDefaultScreen(display); //2 api



	//step3  get default depth from above 2
	defaultDepth = XDefaultDepth(display, defaultScreen);//3API

	//step 4 get visual info from above 3
	memset((void*)&visualInfo,0,sizeof(XVisualInfo));
	status = XMatchVisualInfo(display, defaultScreen, defaultDepth,TrueColor,&visualInfo);
	
	if (status == 0)
	{
		printf("XMatchVisualInfo() Failed\n");
		uninitialise();
		exit(1); //failure exit 

	}
//step 4.set window attribute
	memset((void*)&windowAttributes,0,sizeof(XSetWindowAttributes));
	windowAttributes.border_pixel = 0;
	windowAttributes.background_pixel = XBlackPixel(display, visualInfo.screen);//5API
	windowAttributes.background_pixmap = 0;
	windowAttributes.colormap = XCreateColormap(display,XRootWindow(display, visualInfo.screen), visualInfo.visual,AllocNone);
	
	//step 5.assign this color map to global color map 
	colormap = windowAttributes.colormap;

	//set the stylemask 
	styleMask = CWBorderPixel|CWBackPixel|CWColormap|CWEventMask;

	// 8. Create Window
	window = XCreateWindow(display,
						XRootWindow(display, visualInfo.screen),
						0,
						0,
						WIN_WIDTH,
						WIN_HEIGHT,
						0,
						visualInfo.depth,
						InputOutput,
						visualInfo.visual,
						styleMask,
						&windowAttributes);
	//status = XMatchVisualInfo(display, defaultScreen, defaultDepth, TrueColor, &visualInfo);
	if (!window)
	{
		printf("xCreateWindow() Failed\n");
		uninitialise();
		exit(1); //failure exit 
	}


	//specify to which event this window  to respond 

	XSelectInput(display,window,ExposureMask | VisibilityChangeMask | StructureNotifyMask | KeyPressMask | ButtonPressMask | PointerMotionMask);
	
	//specify window manger delete atom 
	windowManagerDelete = XInternAtom(display,"WM_DELETE_WINDOW",True);

	//Add / Set above atom as protocol for window manager
	XSetWMProtocols(display,window,&windowManagerDelete,1);

	//give caption to window
	XStoreName(display,window,"PANKAJ MUKUND KALE XWindow");

	//show/map the  window
	XMapWindow(display,window);

	//event loop
	while (1)
	{
		XNextEvent(display, &event);
		switch (event.type)
		{
		case KeyPress:

			keySym = XkbKeycodeToKeysym(display, event.xkey.keycode, 0, 0);
			switch (keySym)
			{
			case XK_Escape :
				uninitialise();
				exit(0);
				break;

				default:
				break;

			}
		case 33 : 
			uninitialise();
			exit(0);
			break;
		
			default:
			break;
		}
	}


	uninitialise();
	return (0);
	
}

//define function
void uninitialise(void)
{
	//code
	if (window)
	{
		XDestroyWindow(display, window);

	}
	if (colormap)
	{
		XFreeColormap(display,colormap);
	}
	if (display)
	{
		XCloseDisplay(display);
		display = NULL;
	}
}
