//Standard Header File
#include <stdio.h> // for printf
#include <stdlib.h> // for exit
#include <memory.h> //memset
#include <SOIL/SOIL.h>
#include"OGL.h" //ICON

//x11 header files
#include <X11/Xlib.h> //forall x window API
#include <X11/Xutil.h>//for x visual info and related API
#include <X11/XKBlib.h>

#include <GL/gl.h>
#include <GL/glx.h>
#include <GL/glu.h>

//macros
#define WIN_WIDTH 800
#define WIN_HEIGHT 600

//global variable declaration
Display *display = NULL;
Colormap colormap;
Window window;
XVisualInfo *visualInfo;
FILE *gpFile = NULL;
Bool bFullScreen=False;

Bool bActiveWindow = False;
GLXContext glxContext =NULL;

GLfloat cangle = 0.0f;
GLfloat pangle = 0.0f;

GLuint texture_stone = 0;
GLuint texture_kundali = 0;
//main
int main(void)
{
	//local function declarations
	void uninitialise(void);
	void toggleFullScreen(void);
	void draw(void);
	void update(void);
	int initialize(void);
	void resize(int,int);
	

	//local variable declarations
	int defaultScreen;
	int defaultDepth;	
	Status status;
	XSetWindowAttributes windowAttributes;
	int styleMask;
	Atom windowManagerDelete;
	XEvent event;
	KeySym keySym;
	char keys[26];

	int screenWidth;
	int screenHeight;


	int frameBufferAttributes[]={GLX_DOUBLEBUFFER,True,
                                 GLX_RGBA,
                                 GLX_RED_SIZE,8,
                                 GLX_GREEN_SIZE,8,
                                 GLX_BLUE_SIZE,8,
                                 GLX_ALPHA_SIZE,8,
                                 None};
	
 Bool bDONE= False;
 int iResult = 0;

	
	//code
	//Step 1.open connection with server 
	display = XOpenDisplay(NULL); //1 api
	if (display == NULL)
	{
		printf("XOpenDisplay() Failed\n");
		uninitialise();
		exit(1); //failure exit 
	}
											
	// Step 2.get default screen from above display
	defaultScreen = XDefaultScreen(display); //2 api

	//step3  get default depth from above 2
	visualInfo = glXChooseVisual(display,defaultScreen,frameBufferAttributes);
    if(visualInfo == NULL)
    {
        printf("glXChooseVisual failed\n");
        uninitialise();
        exit(1);
    }
//step 4.set window attribute
	memset((void*)&windowAttributes,0,sizeof(XSetWindowAttributes));
	windowAttributes.border_pixel = 0;
	windowAttributes.background_pixel = XBlackPixel(display, visualInfo->screen);//5API
	windowAttributes.background_pixmap = 0;
	windowAttributes.colormap = XCreateColormap(display,XRootWindow(display, visualInfo->screen), visualInfo->visual,AllocNone);
	
	//step 5.assign this color map to global color map 
	colormap = windowAttributes.colormap;

	//set the stylemask 
	styleMask = CWBorderPixel|CWBackPixel|CWColormap|CWEventMask;

	//now finally create window
	//window = XCreateWindow(display, XRootWindow(display, (visualInfo.screen),0,0,WIN_WIDTH,WIN_HEIGHT,0,visualInfo.depth,InputOutput, visualInfo.visual, styleMask,&windowAttribute));
	
	// 8. Create Window
	window = XCreateWindow(display,
						XRootWindow(display, visualInfo->screen),
						0,
						0,
						WIN_WIDTH,
						WIN_HEIGHT,
						0,
						visualInfo->depth,
						InputOutput,
						visualInfo->visual,
						styleMask,
						&windowAttributes);
                
	if(!window)
	{
		printf("xCreateWindow() Failed\n");
		uninitialise();
		exit(1); //failure exit 
	}
	//specify to which event this window  to respond 

	XSelectInput(display,window,ExposureMask|VisibilityChangeMask|StructureNotifyMask|KeyPressMask|ButtonPressMask|PointerMotionMask|FocusChangeMask);
	
	//specify window manger delete atom 
	windowManagerDelete = XInternAtom(display,"WM_DELETE_WINDOW",True);

	//Add / Set above atom as protocol for window manager
	XSetWMProtocols(display,window,&windowManagerDelete,1);

	//give caption to window
	XStoreName(display,window,"PANKAJ MUKUND KALE");

	//show/map the  window
	XMapWindow(display,window);

//center the window.
screenWidth = XWidthOfScreen(XScreenOfDisplay(display,visualInfo->screen));
screenHeight = XHeightOfScreen(XScreenOfDisplay(display,visualInfo->screen));

XMoveWindow(display,window,(screenWidth-WIN_WIDTH)/2,(screenHeight-WIN_HEIGHT)/2);
//opengl intialization
    iResult = initialize();
    if (iResult != 0)
	{
		printf("initialize() failed");
		exit(1);
	}



	//Event Loop
    while(bDONE==False)
    {   while(XPending(display))
		{

	
        memset((void*)&event, 0, sizeof(XEvent));
        XNextEvent(display,&event);
        switch (event.type)
        {

			case FocusIn:
                bActiveWindow = True;
                break;

            case FocusOut:
                bActiveWindow = False;
                break;

            case ConfigureNotify:
                resize(event.xconfigure.width,event.xconfigure.height);
                break;


        case KeyPress:
             keySym=XkbKeycodeToKeysym(display,event.xkey.keycode,0,0);//WM_KEYDOWN
             switch (keySym)
             {
             case XK_Escape:
                        uninitialise();
                        exit(0);
             break;
             
             default:
                break;
             }

             XLookupString(&event.xkey,keys,sizeof(keys),NULL,NULL);//WM_CHAR
             switch (keys[0])
             {
             case 'F':
             case 'f':
                        if(bFullScreen==False)
                        {
							printf("When keypressed F/f");
                            toggleFullScreen();
                            bFullScreen=True;
                        }
                        else
                        {
                            toggleFullScreen();
                            bFullScreen=False;
                        }
                break;
				
            
             	default:
                break;
             }
        break;
        
        case 33:
                uninitialise();
                exit(0);
        break;
        
        	default:
            break;

        }

		}
		if(bActiveWindow == True)
		{
			draw();
			update();

		}

    }

	
	uninitialise();
	return (0);
	
}



void toggleFullScreen(void)
{

	//local variable decalrations
	Atom windowManagerStateNormal;	
	Atom windowManagerStateFullScreen;
	XEvent event;

	//code

	windowManagerStateNormal=XInternAtom(display,"_NET_WM_STATE",False);
 	
	windowManagerStateFullScreen=XInternAtom(display,"_NET_WM_STATE_FULLSCREEN",False);
	
	//memset the e vent structure and delete with the aboove two item


	memset((void*)&event,0,sizeof(XEvent));
GLuint texture_smiley = 0;
	event.type=ClientMessage;
	event.xclient.window = window;
	event.xclient.message_type=windowManagerStateNormal;
	event.xclient.format = 32;
	event.xclient.data.l[0] = bFullScreen ? 0:1;
	event.xclient.data.l[1]= windowManagerStateFullScreen;

	//send the event
	
	XSendEvent(display,XRootWindow(display,visualInfo->screen),False,SubstructureNotifyMask,&event);

}

int initialize(void)
{
    void resize(int,int);
    Bool bresult=False;
    Bool loadGLTexture(GLuint* texture, const char* fileName);
	//code
    //create opengl context
    glxContext = glXCreateContext(display,visualInfo,NULL,True);
    if(glxContext == NULL)
    {
        printf("In initialize glxCreateContext fialed\n");
        return(-1);
    }
    //make this context as current context
    if(glXMakeCurrent(display,window,glxContext) == False)
    {
        printf("glxMakeCurrent failed\n");
        return(-2);
    }

//usual opengl code
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);    //opengl starts here

	bresult = loadGLTexture(&texture_kundali, "./Vijay_Kundali.bmp");
		if (bresult == False)
		{
			fprintf(gpFile, "loading of kundali texture failed\n");

			return -7;
		}

	bresult = loadGLTexture(&texture_stone, "./Stone.bmp");
		if (bresult == False)
		{
			fprintf(gpFile, "loading of kundali texture failed\n");

			return -8;
		}
		//tell opengl to the enable texture.
		//4TH STEP HERE 
		glEnable(GL_TEXTURE_2D);

    
    //warmup resize
    resize(WIN_WIDTH,WIN_HEIGHT);
    return(0);
}


Bool loadGLTexture(GLuint* texture, const char* fileName)
{
	// local variable declarations
	int width, height;
	unsigned char* imageData = NULL;

	// Load the image
	imageData = SOIL_load_image(fileName, &width, &height, NULL, SOIL_LOAD_RGB);
	if(imageData == NULL)
	{
		return False;
	}

	// Create OpenGL texture object
	glGenTextures(1, texture);

	// Bind to the generated texture
	glBindTexture(GL_TEXTURE_2D, *texture);

	// Decide image pixel alignment and unpacking
	glPixelStorei(GL_UNPACK_ALIGNMENT, 4);

	// Set texture parameters
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);

	// Accordingly use image data and texture parameters to 
	// create multiple such images which will map with 
	// objects in the scene to render
	gluBuild2DMipmaps(GL_TEXTURE_2D, 3, width, height, 
		GL_RGB, GL_UNSIGNED_BYTE, (void*)imageData);

	// Unbind with OpenGL texture object
	glBindTexture(GL_TEXTURE_2D, 0);

	// Delete image object
	SOIL_free_image_data(imageData);

	return True;
}

void resize(int width,int height)
{
    //code
    if(height <= 0)
    {
	    height = 1;
	}

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
	gluPerspective(45.0f, (GLfloat)width / (GLfloat)height,0.1f,100.0f);
}
void draw(void)
{
   glClear(GL_COLOR_BUFFER_BIT |GL_DEPTH_BUFFER_BIT);//blue colour bhint zhali ithe
	glMatrixMode(GL_MODELVIEW); // model view matri tayar hote internall ithe
	glLoadIdentity(); // identity matrix tayar hote inrernally ithe
	glTranslatef(-1.5f,0.0f,-6.0f);
	glRotatef(pangle,0.0f,1.0f,0.0f);
	glBindTexture(GL_TEXTURE_2D, texture_stone);
	glBegin(GL_TRIANGLES);

	//front face
	//glColor3f(1.0f,0.0f,0.0f);
	   //1
	glTexCoord2f(0.5, 1);
	glVertex3f(0.0f, 1.0f, 0.0f);

	//left bottom
//	glColor3f(1.0f, 0.0f, 0.0f)
	  
	//glColor3f(0.0f, 1.0f, 0.0f);
	glTexCoord2f(0.0, 0.0);
	glVertex3f(-1.0f, -1.0f, 1.0f);

	//right bottom
	//glColor3f(1.0f, 0.0f, 0.0f);
	
	//glColor3f(0.0f, 0.0f, 1.0f);
	glTexCoord2f(1.0, 0.0);
	glVertex3f(1.0f, -1.0f, 1.0f);


	//right face

	//glColor3f(1.0f, 0.0f, 0.0f);
	glTexCoord2f(0.5, 1.0);
	glVertex3f(0.0f, 1.0f, 0.0f);

	//glColor3f(0.0f, 0.0f, 1.0f);
	glTexCoord2f(1.0, 0.0);
	glVertex3f(1.0f, -1.0f, 1.0f);
	//glColor3f(0.0f, 1.0f, 0.0f);
	glTexCoord2f(0.0, 0.0);
	glVertex3f(1.0f, -1.0f, -1.0f);


	//back face
	//glColor3f(1.0f, 0.0f, 0.0f);
	glTexCoord2f(0.5, 1.0);
	glVertex3f(0.0f, 1.0f, 0.0f);
	
	//glColor3f(0.0f, 1.0f, 0.0f);
	glTexCoord2f(1.0, 0.0);
	glVertex3f(1.0f, -1.0f, -1.0f);
	//glColor3f(0.0f, 0.0f, 1.0f);
	glTexCoord2f(0.0, 0.0);
	glVertex3f(-1.0f, -1.0f, -1.0f);


	//left face
	
	//glColor3f(1.0f, 0.0f, 0.0f);
	glTexCoord2f(0.5, 1.0);
	glVertex3f(0.0f, 1.0f, 0.0f);
	
//	glColor3f(0.0f, 0.0f, 1.0f);
	glTexCoord2f(0.0, 0.0);
	glVertex3f(-1.0f, -1.0f, -1.0f);

	//	glColor3f(0.0f, 1.0f, 0.0f);
	glTexCoord2f(1.0, 0.0);
	glVertex3f(-1.0f, -1.0f, 1.0f);
	glEnd();
	glBindTexture(GL_TEXTURE_2D,0);

	glLoadIdentity(); // identity matrix tayar hote inrernally ithe
	glTranslatef(1.5f, 0.0f, -6.0f);
	glScalef(0.75f,0.75f,0.75f);
	glRotatef(cangle, 1.0f, 0.0f, 0.0f);// x rotation 
	glRotatef(cangle, 0.0f, 1.0f, 0.0f); //y rotation
	glRotatef(cangle, 0.0f, 0.0f, 1.0f); // z rotation
	
	glBindTexture(GL_TEXTURE_2D, texture_kundali);
	glBegin(GL_QUADS);
	//front face
	//RT 
	
	glTexCoord2f(0.0, 0.0); /////1
	//glColor3f(1.0f, 0.0f, 0.0f);//red colour
	glVertex3f(1.0f, 1.0f, 1.0f); //front face so z coordinate 1.0f.

	//LT
	glTexCoord2f(1.0, 0.0);/////2
	glVertex3f(-1.0f, 1.0f, 1.0f);

	//LB
	glTexCoord2f(1.0, 1.0);/////3
	glVertex3f(-1.0f, -1.0f, 1.0f);

	//RB
	glTexCoord2f(0.0, 1.0);//////4
	glVertex3f(1.0f, -1.0f, 1.0f);
	//###########################################
	//right face
	//right top 
	
	
	//glColor3f(0.0f, 1.0f, 0.0f);//green colour
	glTexCoord2f(1.0, 1.0);//5
	glVertex3f(1.0f, 1.0f, -1.0f);

	//left top
	glTexCoord2f(1.0, 0.0);//6
	glVertex3f(1.0f, 1.0f, 1.0f);

	//left bottom
	glTexCoord2f(1.0, 1.0);//7
	glVertex3f(1.0f, -1.0f, 1.0f);

	//right bottom
	glTexCoord2f(0.0, 1.0);//8
	glVertex3f(1.0f, -1.0f, -1.0f);
	//#################################################
		//back face
		//right top 
	glTexCoord2f(0.0, 0.0);//9
		//glColor3f(0.0f, 0.0f, 1.0f);//blue coloour
	glVertex3f(-1.0f, 1.0f, -1.0f);

	//left top
	glTexCoord2f(1.0, 0.0);//10
	glVertex3f(1.0f, 1.0f, -1.0f);

	//left bottom
	glTexCoord2f(1.0, 1.0);//11
	glVertex3f(1.0f, -1.0f, -1.0f);

	//right bottom
	glTexCoord2f(0.0, 1.0);//12
	glVertex3f(-1.0f, -1.0f, -1.0f);
	//##############################################
		//left face
		//right top 
	glTexCoord2f(0.0, 0.0);//13
		//glColor3f(0.0f, 1.0f, 1.0f);//cyan colour
	glVertex3f(-1.0f, -1.0f, 1.0f);

	//left top
	glTexCoord2f(1.0, 0.0);//14
	glVertex3f(-1.0f, 1.0f, -1.0f);

	//left bottom
	glTexCoord2f(1.0, 1.0);//15
	glVertex3f(-1.0f, -1.0f, -1.0f);

	//right bottom
	glTexCoord2f(0.0, 1.0);//16
	glVertex3f(-1.0f, 1.0f, 1.0f);
	//##############################################
		//top face
		//right top 
	glTexCoord2f(0.0, 0.0);//17
		//glColor3f(1.0f, 0.0f, 1.0f);//magneta colour
	glVertex3f(1.0f, 1.0f, -1.0f);

	//left top
	glTexCoord2f(1.0, 0.0);//18
	glVertex3f(-1.0f, 1.0f, -1.0f);

	//left bottom
	glTexCoord2f(1.0, 1.0);//19
	glVertex3f(-1.0f, 1.0f, 1.0f);

	//right bottom
	glTexCoord2f(0.0, 1.0);//20
	glVertex3f(1.0f, 1.0f, 1.0f);

	//########################################
	//bottom face
	//right top
	glTexCoord2f(0.0, 0.0);//21
	//glColor3f(1.0f, 1.0f, 0.0f);//yellow colour
	glVertex3f(1.0f, -1.0f, -1.0f);

	//left top
	glTexCoord2f(1.0, 0.0);//22
	glVertex3f(-1.0f, -1.0f, -1.0f);

	//left bottom
	glTexCoord2f(1.0, 1.0);//23
	glVertex3f(-1.0f, -1.0f, 1.0f);

	//right bottom
	glTexCoord2f(0.0, 1.0);//24
	glVertex3f(1.0f, -1.0f, 1.0f);

	glEnd();
	glBindTexture(GL_TEXTURE_2D, 0);


    glXSwapBuffers(display,window);
}
void update (void)
{
	//code
	pangle = pangle + 0.02f;
	if (pangle > 360.0f)
	{
		pangle = pangle - 360.0f;

		
	}
	cangle = cangle - 0.02f;
	if (cangle < 0.0f)
	{
		cangle = cangle + 360.0f;
	}
}
//define function
void uninitialise(void)
{
	//code

	GLXContext currentGLXContext = NULL;

    //code
    if(visualInfo)
    {
        free(visualInfo);
        visualInfo =  NULL;
    }
    //uncurrent the context
    currentGLXContext = glXGetCurrentContext();
    if(currentGLXContext!=NULL && currentGLXContext == glxContext)
    {
        glXMakeCurrent(display,0,0);
    }

    if(glxContext)
    {
        glXDestroyContext(display,glxContext);
        glxContext = NULL;
    }

	if (window)
	{
		XDestroyWindow(display, window);

	}

	if (colormap)
	{
		XFreeColormap(display,colormap);
	}

if (texture_kundali)
	{
		glDeleteTextures(1,&texture_kundali);
		texture_kundali = 0;
	}

if (texture_stone)
	{
		glDeleteTextures(1, &texture_stone);
		texture_stone = 0;
	}
	if (display)
	{
		XCloseDisplay(display);
		display = NULL;
	}


}
