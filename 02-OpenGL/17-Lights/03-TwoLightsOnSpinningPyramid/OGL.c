//Standard Header File
#include <stdio.h> // for printf
#include <stdlib.h> // for exit
#include <memory.h> //memset

//x11 header files
#include <X11/Xlib.h> //forall x window API
#include <X11/Xutil.h>//for x visual info and related API
#include <X11/XKBlib.h>

#include <GL/gl.h>
#include <GL/glx.h>
#include <GL/glu.h>

//macros
#define WIN_WIDTH 800
#define WIN_HEIGHT 600

//global variable declaration
Display *display = NULL;
Colormap colormap;
Window window;
XVisualInfo *visualInfo;

Bool bFullScreen=False;

Bool bActiveWindow = False;
GLXContext glxContext =NULL;

//GLfloat tangle = 0.0f;
GLfloat cangle = 0.0f;
Bool bLight = False;
GLfloat lightAmbientZero[] = {0.0f,0.0f,0.0f,1.0f};
GLfloat lightDiffuseZero[] = {1.0f,0.0f,0.0f,1.0f};
GLfloat lightSpecularZero[] = {1.0f,0.0f,0.0f,1.0f};
GLfloat lightPositionZero[] = { 4.0f };

GLfloat lightAmbientOne[] = { 0.0f,0.0f,0.0f,1.0f };
GLfloat lightDiffuseOne[] = { 0.0f,1.0f,0.0f,1.0f };
GLfloat lightSpecularOne[] = { 1.0f,0.0f,0.0f,1.0f };
GLfloat lightPositionOne[] = { 4.0f };

GLfloat materialAmbient[] = { 0.0f,0.0f,0.0f, 1.0f};
GLfloat materialDiffused[] = { 1.0f,1.0f,1.0f, 1.0f };
GLfloat materialSpecular[] = { 1.0f,1.0f,1.0f, 1.0f };
GLfloat materialShininess[] = { 128.0f};

GLfloat lightAngleZero = 0.0f;
GLfloat lightAngleOne = 0.0f;
GLfloat lightAngleTwo = 0.0f;


GLUquadric* quadric = NULL;

//main
int main(void)
{
	//local function declarations
	void uninitialise(void);
	void toggleFullScreen(void);
	void draw(void);
	void update(void);
	int initialize(void);
	void resize(int,int);

	//local variable declarations
	int defaultScreen;
	int defaultDepth;	
	Status status;
	XSetWindowAttributes windowAttributes;
	int styleMask;
	Atom windowManagerDelete;
	XEvent event;
	KeySym keySym;
	char keys[26];

	int screenWidth;
	int screenHeight;


	int frameBufferAttributes[]={GLX_DOUBLEBUFFER,True,
                                 GLX_RGBA,
                                 GLX_RED_SIZE,8,
                                 GLX_GREEN_SIZE,8,
                                 GLX_BLUE_SIZE,8,
                                 GLX_ALPHA_SIZE,8,
                                 None};
	
 Bool bDONE= False;
 int iResult = 0;

	
	//code
	//Step 1.open connection with server 
	display = XOpenDisplay(NULL); //1 api
	if (display == NULL)
	{
		printf("XOpenDisplay() Failed\n");
		uninitialise();
		exit(1); //failure exit 
	}
											
	// Step 2.get default screen from above display
	defaultScreen = XDefaultScreen(display); //2 api

	//step3  get default depth from above 2
	visualInfo = glXChooseVisual(display,defaultScreen,frameBufferAttributes);
    if(visualInfo == NULL)
    {
        printf("glXChooseVisual failed\n");
        uninitialise();
        exit(1);
    }
//step 4.set window attribute
	memset((void*)&windowAttributes,0,sizeof(XSetWindowAttributes));
	windowAttributes.border_pixel = 0;
	windowAttributes.background_pixel = XBlackPixel(display, visualInfo->screen);//5API
	windowAttributes.background_pixmap = 0;
	windowAttributes.colormap = XCreateColormap(display,XRootWindow(display, visualInfo->screen), visualInfo->visual,AllocNone);
	
	//step 5.assign this color map to global color map 
	colormap = windowAttributes.colormap;

	//set the stylemask 
	styleMask = CWBorderPixel|CWBackPixel|CWColormap|CWEventMask;

	//now finally create window
	//window = XCreateWindow(display, XRootWindow(display, (visualInfo.screen),0,0,WIN_WIDTH,WIN_HEIGHT,0,visualInfo.depth,InputOutput, visualInfo.visual, styleMask,&windowAttribute));
	
	// 8. Create Window
	window = XCreateWindow(display,
						XRootWindow(display, visualInfo->screen),
						0,
						0,
						WIN_WIDTH,
						WIN_HEIGHT,
						0,
						visualInfo->depth,
						InputOutput,
						visualInfo->visual,
						styleMask,
						&windowAttributes);
                
	if(!window)
	{
		printf("xCreateWindow() Failed\n");
		uninitialise();
		exit(1); //failure exit 
	}
	//specify to which event this window  to respond 

	XSelectInput(display,window,ExposureMask|VisibilityChangeMask|StructureNotifyMask|KeyPressMask|ButtonPressMask|PointerMotionMask|FocusChangeMask);
	
	//specify window manger delete atom 
	windowManagerDelete = XInternAtom(display,"WM_DELETE_WINDOW",True);

	//Add / Set above atom as protocol for window manager
	XSetWMProtocols(display,window,&windowManagerDelete,1);

	//give caption to window
	XStoreName(display,window,"PANKAJ MUKUND KALE");

	//show/map the  window
	XMapWindow(display,window);

//center the window.
screenWidth = XWidthOfScreen(XScreenOfDisplay(display,visualInfo->screen));
screenHeight = XHeightOfScreen(XScreenOfDisplay(display,visualInfo->screen));

XMoveWindow(display,window,(screenWidth-WIN_WIDTH)/2,(screenHeight-WIN_HEIGHT)/2);
//opengl intialization
    iResult = initialize();
    if (iResult != 0)
	{
		printf("initialize() failed");
		exit(1);
	}



	//Event Loop
    while(bDONE==False)
    {   while(XPending(display))
		{

	
        memset((void*)&event, 0, sizeof(XEvent));
        XNextEvent(display,&event);
        switch (event.type)
        {

			case FocusIn:
                bActiveWindow = True;
                break;

            case FocusOut:
                bActiveWindow = False;
                break;

            case ConfigureNotify:
                resize(event.xconfigure.width,event.xconfigure.height);
                break;


        case KeyPress:
             keySym=XkbKeycodeToKeysym(display,event.xkey.keycode,0,0);//WM_KEYDOWN
             switch (keySym)
             {
             case XK_Escape:
                        uninitialise();
                        exit(0);
             break;
             
             default:
                break;
             }

             XLookupString(&event.xkey,keys,sizeof(keys),NULL,NULL);//WM_CHAR
             switch (keys[0])
             {
             case 'F':
             case 'f':
                        if(bFullScreen==False)
                        {
							
                            toggleFullScreen();
                            bFullScreen=True;
                        }
                        else
                        {
                            toggleFullScreen();
                            bFullScreen=False;
                        }
                break;
				
				case 'L':
             	case 'l':
                        if (bLight == False)
						{
							glEnable(GL_LIGHTING);
							bLight = True;
						}
					else
						{		
							glDisable(GL_LIGHTING);
							bLight = False;
						}
                break;
            
             	default:
                break;
             }
        break;
        
        case 33:
                uninitialise();
                exit(0);
        break;
        
        	default:
            break;

        }

		}
		if(bActiveWindow == True)
		{
			draw();
			update();

		}

    }

	
	uninitialise();
	return (0);
	
}



void toggleFullScreen(void)
{

	//local variable decalrations
	Atom windowManagerStateNormal;	
	Atom windowManagerStateFullScreen;
	XEvent event;

	//code

	windowManagerStateNormal=XInternAtom(display,"_NET_WM_STATE",False);
 	
	windowManagerStateFullScreen=XInternAtom(display,"_NET_WM_STATE_FULLSCREEN",False);
	
	//memset the e vent structure and delete with the aboove two item


	memset((void*)&event,0,sizeof(XEvent));

	event.type=ClientMessage;
	event.xclient.window = window;
	event.xclient.message_type=windowManagerStateNormal;
	event.xclient.format = 32;
	event.xclient.data.l[0] = bFullScreen ? 0:1;
	event.xclient.data.l[1]= windowManagerStateFullScreen;

	//send the event
	
	XSendEvent(display,XRootWindow(display,visualInfo->screen),False,SubstructureNotifyMask,&event);

}

int initialize(void)
{
    void resize(int,int);
    
    //code
    //create opengl context
    glxContext = glXCreateContext(display,visualInfo,NULL,True);
    if(glxContext == NULL)
    {
        printf("In initialize glxCreateContext fialed\n");
        return(-1);
    }
    //make this context as current context
    if(glXMakeCurrent(display,window,glxContext) == False)
    {
        printf("glxMakeCurrent failed\n");
        return(-2);
    }

//usual opengl code
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);    //opengl starts here
    
//Initialising light0
	glLightfv(GL_LIGHT0,GL_AMBIENT,lightAmbientZero);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, lightDiffuseZero);
	glLightfv(GL_LIGHT0, GL_SPECULAR, lightSpecularZero);
	glEnable(GL_LIGHT0);

	//initilaising light1
	glLightfv(GL_LIGHT1, GL_AMBIENT, lightAmbientOne);
	glLightfv(GL_LIGHT1, GL_DIFFUSE, lightDiffuseOne);
	glLightfv(GL_LIGHT1, GL_SPECULAR, lightSpecularOne);
	glEnable(GL_LIGHT1);

	//material initialisation
	glMaterialfv(GL_FRONT,GL_AMBIENT,materialAmbient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffused);
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);
	glMaterialfv(GL_FRONT,GL_SHININESS,materialShininess);


	//initialise quadric
	quadric = gluNewQuadric();
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

	glEnable(GL_LIGHT0);




    //warmup resize
    resize(WIN_WIDTH,WIN_HEIGHT);
    return(0);
}
void resize(int width,int height)
{
    //code
    if(height <= 0)
    {
	    height = 1;
	}

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
	gluPerspective(45.0f, (GLfloat)width / (GLfloat)height,0.1f,100.0f);
}
void draw(void)
{
    //code
   	glClear(GL_COLOR_BUFFER_BIT |GL_DEPTH_BUFFER_BIT);//blue colour bhint zhali ithe
	glMatrixMode(GL_MODELVIEW); // model view matri tayar hote internall ithe
	glLoadIdentity(); // identity matrix tayar hote inrernally ithe
	
	//camera transformation 
	glPushMatrix();
	gluLookAt(0.0f, 0.0f, 3.0f,0.0f,0.0f,0.0f,0.0f,1.0f,0.0f);

	//rendering light0(x rotation)

	glPushMatrix();
	glRotatef(lightAngleZero,1.0f,0.0f,0.0f);

	lightPositionZero[0] = 0.0f;//rule
	lightPositionZero[1] = 0.0f;//x notloose but lightPositionZero[] = ;
	lightPositionZero[2] = lightAngleZero;
	lightPositionZero[3] = 1.0f;

	glLightfv(GL_LIGHT0,GL_POSITION,lightPositionZero);
	glPopMatrix();


	//rendering light1
	glPushMatrix();
	glRotatef(lightAngleOne, 0.0f, 1.0f, 0.0f);
	lightPositionOne[0] = lightAngleOne;// notloose but lightPositionZero[] = ;
	lightPositionOne[1] = 0.0f;//by rule
	lightPositionOne[2] = 0.0f;
	lightPositionOne[3] = 1.0f;


	glLightfv(GL_LIGHT1, GL_POSITION, lightPositionOne);
	glPopMatrix();

	
	
	gluSphere(quadric, 0.2f, 50, 50);//glusphere call internally create all normals for it
	//glPolygonMode(GL_FRONT_AND_BACK,GL_FILL);



    glXSwapBuffers(display,window);
}
void update (void)
{
	//code
//Animating light 0

	lightAngleZero = lightAngleZero + 0.01f;

	if (lightAngleZero > 360.0f)
	{
		lightAngleZero = lightAngleZero - 360.0f;
	}


	//Animating light 1

	lightAngleOne = lightAngleOne + 1.0f;

	if (lightAngleOne > 360.0f)
	{
		lightAngleOne = lightAngleOne - 360.0f;
	}
	
}
//define function
void uninitialise(void)
{
	//code

	GLXContext currentGLXContext = NULL;

    //code
    if(visualInfo)
    {
        free(visualInfo);
        visualInfo =  NULL;
    }
    //uncurrent the context
    currentGLXContext = glXGetCurrentContext();
    if(currentGLXContext!=NULL && currentGLXContext == glxContext)
    {
        glXMakeCurrent(display,0,0);
    }

    if(glxContext)
    {
        glXDestroyContext(display,glxContext);
        glxContext = NULL;
    }

	if (window)
	{
		XDestroyWindow(display, window);

	}

	if (colormap)
	{
		XFreeColormap(display,colormap);
	}

	if (quadric)
	{
		gluDeleteQuadric(quadric);
		quadric = NULL;
	}
	if (display)
	{
		XCloseDisplay(display);
		display = NULL;
	}


}
