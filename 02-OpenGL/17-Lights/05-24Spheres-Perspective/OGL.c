//Standard Header File
#include <stdio.h> // for printf
#include <stdlib.h> // for exit
#include <memory.h> //memset

#include"OGL.h" 

//x11 header files
#include <X11/Xlib.h> //forall x window API
#include <X11/Xutil.h>//for x visual info and related API
#include <X11/XKBlib.h>

#include <GL/gl.h>
#include <GL/glx.h>
#include <GL/glu.h>

//macros
#define WIN_WIDTH 800
#define WIN_HEIGHT 600

//global variable declaration
Display *display = NULL;
Colormap colormap;
Window window;
XVisualInfo *visualInfo;

Bool bFullScreen=False;

Bool bActiveWindow = False;
GLXContext glxContext =NULL;

//GLfloat tangle = 0.0f;
//GLfloat cangle = 0.0f;
Bool bLight = False;

GLfloat lightAmbient[] = { 0.0f,0.0f,0.0f,1.0f };
GLfloat lightDiffuse[] = { 1.0f,1.0f,1.0f,1.0f };//white
GLfloat lightPosition[] = { 0.0f,0.0f,0.0f,1.0f };//positional light not directional 


GLUquadric* quadric = NULL;

GLfloat angleForXRotation = 0.0f;
GLfloat angleForYRotation = 0.0f;
GLfloat angleForZRotation = 0.0f;

GLuint keyPressed = 0;

//main
int main(void)
{
	//local function declarations
	void uninitialise(void);
	void toggleFullScreen(void);
	void draw(void);
	void update(void);
	int initialize(void);
	void resize(int,int);

	//local variable declarations
	int defaultScreen;
	int defaultDepth;	
	Status status;
	XSetWindowAttributes windowAttributes;
	int styleMask;
	Atom windowManagerDelete;
	XEvent event;
	KeySym keySym;
	char keys[26];

	int screenWidth;
	int screenHeight;


	int frameBufferAttributes[]={GLX_DOUBLEBUFFER,True,
                                 GLX_RGBA,
                                 GLX_RED_SIZE,8,
                                 GLX_GREEN_SIZE,8,
                                 GLX_BLUE_SIZE,8,
                                 GLX_ALPHA_SIZE,8,
                                 None};
	
 Bool bDONE= False;
 int iResult = 0;

	
	//code
	//Step 1.open connection with server 
	display = XOpenDisplay(NULL); //1 api
	if (display == NULL)
	{
		printf("XOpenDisplay() Failed\n");
		uninitialise();
		exit(1); //failure exit 
	}
											
	// Step 2.get default screen from above display
	defaultScreen = XDefaultScreen(display); //2 api

	//step3  get default depth from above 2
	visualInfo = glXChooseVisual(display,defaultScreen,frameBufferAttributes);
    if(visualInfo == NULL)
    {
        printf("glXChooseVisual failed\n");
        uninitialise();
        exit(1);
    }
//step 4.set window attribute
	memset((void*)&windowAttributes,0,sizeof(XSetWindowAttributes));
	windowAttributes.border_pixel = 0;
	windowAttributes.background_pixel = XBlackPixel(display, visualInfo->screen);//5API
	windowAttributes.background_pixmap = 0;
	windowAttributes.colormap = XCreateColormap(display,XRootWindow(display, visualInfo->screen), visualInfo->visual,AllocNone);
	
	//step 5.assign this color map to global color map 
	colormap = windowAttributes.colormap;

	//set the stylemask 
	styleMask = CWBorderPixel|CWBackPixel|CWColormap|CWEventMask;

	//now finally create window
	//window = XCreateWindow(display, XRootWindow(display, (visualInfo.screen),0,0,WIN_WIDTH,WIN_HEIGHT,0,visualInfo.depth,InputOutput, visualInfo.visual, styleMask,&windowAttribute));
	
	// 8. Create Window
	window = XCreateWindow(display,
						XRootWindow(display, visualInfo->screen),
						0,
						0,
						WIN_WIDTH,
						WIN_HEIGHT,
						0,
						visualInfo->depth,
						InputOutput,
						visualInfo->visual,
						styleMask,
						&windowAttributes);
                
	if(!window)
	{
		printf("xCreateWindow() Failed\n");
		uninitialise();
		exit(1); //failure exit 
	}
	//specify to which event this window  to respond 

	XSelectInput(display,window,ExposureMask|VisibilityChangeMask|StructureNotifyMask|KeyPressMask|ButtonPressMask|PointerMotionMask|FocusChangeMask);
	
	//specify window manger delete atom 
	windowManagerDelete = XInternAtom(display,"WM_DELETE_WINDOW",True);

	//Add / Set above atom as protocol for window manager
	XSetWMProtocols(display,window,&windowManagerDelete,1);

	//give caption to window
	XStoreName(display,window,"PANKAJ MUKUND KALE");

	//show/map the  window
	XMapWindow(display,window);

//center the window.
screenWidth = XWidthOfScreen(XScreenOfDisplay(display,visualInfo->screen));
screenHeight = XHeightOfScreen(XScreenOfDisplay(display,visualInfo->screen));

XMoveWindow(display,window,(screenWidth-WIN_WIDTH)/2,(screenHeight-WIN_HEIGHT)/2);
//opengl intialization
    iResult = initialize();
    if (iResult != 0)
	{
		printf("initialize() failed");
		exit(1);
	}



	//Event Loop
    while(bDONE==False)
    {   while(XPending(display))
		{

	
        memset((void*)&event, 0, sizeof(XEvent));
        XNextEvent(display,&event);
        switch (event.type)
        {

			case FocusIn:
                bActiveWindow = True;
                break;

            case FocusOut:
                bActiveWindow = False;
                break;

            case ConfigureNotify:
                resize(event.xconfigure.width,event.xconfigure.height);
                break;


        case KeyPress:
             keySym=XkbKeycodeToKeysym(display,event.xkey.keycode,0,0);//WM_KEYDOWN
             switch (keySym)
             {
             case XK_Escape:
                        uninitialise();
                        exit(0);
             break;
             
             default:
                break;
             }

             XLookupString(&event.xkey,keys,sizeof(keys),NULL,NULL);//WM_CHAR
             switch (keys[0])
             {
             case 'F':
             case 'f':
                        if(bFullScreen==False)
                        {
							
                            toggleFullScreen();
                            bFullScreen=True;
                        }
                        else
                        {
                            toggleFullScreen();
                            bFullScreen=False;
                        }
                break;
				
				case 'L':
             	case 'l':
                        if(bLight==False)
                        {
							
                           glEnable(GL_LIGHTING);
                            bLight=True;
                        }
                        else
                        {
                            glDisable(GL_LIGHTING);
                            bLight=False;
                        }
                break;

		case 'X':
		case 'x':
			keyPressed = 1;
			angleForXRotation = 0.0f; //Reset
			break;

		case 'Y':
		case 'y':
			keyPressed = 2;
			angleForYRotation = 0.0f; //Reset
			break;

		case 'Z':
		case 'z':
			keyPressed = 3;
			angleForZRotation = 0.0f; //Reset
			break;
            
             	default:
                break;
             }
        break;
        
        case 33:
                uninitialise();
                exit(0);
        break;
        
        	default:
            break;

        }

		}
		if(bActiveWindow == True)
		{
			draw();
			update();

		}

    }

	
	uninitialise();
	return (0);
	
}



void toggleFullScreen(void)
{

	//local variable decalrations
	Atom windowManagerStateNormal;	
	Atom windowManagerStateFullScreen;
	XEvent event;

	//code

	windowManagerStateNormal=XInternAtom(display,"_NET_WM_STATE",False);
 	
	windowManagerStateFullScreen=XInternAtom(display,"_NET_WM_STATE_FULLSCREEN",False);
	
	//memset the e vent structure and delete with the aboove two item


	memset((void*)&event,0,sizeof(XEvent));

	event.type=ClientMessage;
	event.xclient.window = window;
	event.xclient.message_type=windowManagerStateNormal;
	event.xclient.format = 32;
	event.xclient.data.l[0] = bFullScreen ? 0:1;
	event.xclient.data.l[1]= windowManagerStateFullScreen;

	//send the event
	
	XSendEvent(display,XRootWindow(display,visualInfo->screen),False,SubstructureNotifyMask,&event);

}

int initialize(void)
{
    void resize(int,int);
    
    //code
    //create opengl context
    glxContext = glXCreateContext(display,visualInfo,NULL,True);
    if(glxContext == NULL)
    {
        printf("In initialize glxCreateContext fialed\n");
        return(-1);
    }
    //make this context as current context
    if(glXMakeCurrent(display,window,glxContext) == False)
    {
        printf("glxMakeCurrent failed\n");
        return(-2);
    }

//usual opengl code
    glClearColor(1.0f, 0.0f, 0.0f, 1.0f);    //opengl starts here
    
	glLightfv(GL_LIGHT0,GL_AMBIENT,lightAmbient);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, lightDiffuse);
	glEnable(GL_LIGHT0);


	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	quadric = gluNewQuadric();

    //warmup resize
    resize(WIN_WIDTH,WIN_HEIGHT);
    return(0);
}
void resize(int width,int height)
{
    //code
    if (height <= 0)
	{
		height = 1;
	}
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	//glViewport(0, 0, (GLsizei)width, (GLsizei)height);
	if (width <= height)
	{
		glOrtho(0.0f, 15.5f,0.0f * ((GLfloat)height / (GLfloat)width), 15.5f * ((GLfloat)height / (GLfloat)width), -10.0f, 10.0f);//lrbtf
	}
	else
	{
		glOrtho(0.0f * ((GLfloat)width / (GLfloat)height), 15.5f * ((GLfloat)width / (GLfloat)height), 0.0f, 15.0f, -10.0f, 10.0f);
		glViewport(0, 0, (GLsizei)width, (GLsizei)height);
	}
}

void draw(void)
{
  //These are Four local variable declared which are updated here
	GLfloat materialAmbient[4];
	GLfloat materialDiffuse[4];
	GLfloat materialSpecular[4];
	GLfloat Shininess;

	glClear(GL_COLOR_BUFFER_BIT |GL_DEPTH_BUFFER_BIT);//blue colour bhint zhali ithe
	glMatrixMode(GL_MODELVIEW); // model view matri tayar hote internall ithe
	glLoadIdentity(); // identity matrix tayar hote inrernally ithe
	
	//Animation 
	 
	if (keyPressed == 1)
	{
		glRotatef(angleForXRotation, 1.0f, 0.0f, 0.0f);// For x direction rotation after this angleForXRotation first value changes.
		lightPosition[2] = angleForXRotation;
	}
	if (keyPressed == 2)
	{
		glRotatef(angleForYRotation, 0.0f, 1.0f, 0.0f); // For y direction rotation after this angleForYRotation Second value changes.
		lightPosition[0] = angleForYRotation;
	}
	if (keyPressed == 3)
	{
		glRotatef(angleForZRotation, 0.0f, 0.0f, 1.0f);// For z direction rotation after this angleForZRotation Second value changes.
		lightPosition[1] = angleForZRotation;
	}

	glLightfv(GL_LIGHT0,GL_POSITION,lightPosition);

	// ***** 1st sphere on 1st column, emerald ***** First Column of precious stone
	// emerald material 
	materialAmbient[0]= 0.0215;
	materialAmbient[1] = 0.1745;
	materialAmbient[2] = 0.0215;
	materialAmbient[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);


	materialDiffuse[0]= 0.07568;
	materialDiffuse[1] = 0.61424;
	materialDiffuse[2] = 0.07568;
	materialDiffuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);

	materialSpecular[0] = 0.633;
	materialSpecular[1] = 0.727811;
	materialSpecular[2] = 0.633;
	materialSpecular[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);

	Shininess = 0.6 * 128;
	glMaterialf(GL_FRONT, GL_SHININESS, Shininess);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(1.5f,14.0f,0.0f);
	
	
	gluSphere(quadric, 1.0f, 30, 30);//glusphere call internally create all normals for it
	
	//***** 2nd sphere on 1st column, jade *****
	materialAmbient[0] = 0.135;
	materialAmbient[1] = 0.2225;
	materialAmbient[2] = 0.1575;
	materialAmbient[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);


	materialDiffuse[0] = 0.54;
	materialDiffuse[1] = 0.89;
	materialDiffuse[2] = 0.63;
	materialDiffuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);

	materialSpecular[0] = 0.316228;
	materialSpecular[1] = 0.316228;
	materialSpecular[2] = 0.316228;
	materialSpecular[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);

	Shininess = 0.1 * 128;
	glMaterialf(GL_FRONT, GL_SHININESS, Shininess);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(1.5f, 11.5f, 0.0f);


	gluSphere(quadric, 1.0f, 30, 30);//glusphere call internally create all normals for it


	//***** 3rd sphere on 1st column, obsidian *****
	materialAmbient[0] = 0.05375;
	materialAmbient[1] = 0.05;
	materialAmbient[2] = 0.06625;
	materialAmbient[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);


	materialDiffuse[0] = 0.18275;
	materialDiffuse[1] = 0.17;
	materialDiffuse[2] = 0.22525;
	materialDiffuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);

	materialSpecular[0] = 0.332741;
	materialSpecular[1] = 0.328634;
	materialSpecular[2] = 0.346435;
	materialSpecular[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);

	Shininess = 0.3 * 128;
	glMaterialf(GL_FRONT, GL_SHININESS, Shininess);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(1.5f, 9.0f, 0.0f);


	gluSphere(quadric, 1.0f, 30, 30);//glusphere call internally create all normals for it

	//***** 4th sphere on 1st column, pearl *****
	materialAmbient[0] = 0.25;
	materialAmbient[1] = 0.20725;
	materialAmbient[2] = 0.20725;
	materialAmbient[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);


	materialDiffuse[0] = 1.0;
	materialDiffuse[1] = 0.829;
	materialDiffuse[2] = 0.829;
	materialDiffuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);

	materialSpecular[0] = 0.296648;
	materialSpecular[1] = 0.296648;
	materialSpecular[2] = 0.296648;
	materialSpecular[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);

	Shininess = 0.088 * 128;
	glMaterialf(GL_FRONT, GL_SHININESS, Shininess);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(1.5f, 6.5f, 0.0f);


	gluSphere(quadric, 1.0f, 30, 30);//glusphere call internally create all normals for it


	//***** 5th sphere on 1st column, ruby *****
	materialAmbient[0] = 0.1745;
	materialAmbient[1] = 0.01175;
	materialAmbient[2] = 0.01175;
	materialAmbient[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);


	materialDiffuse[0] = 0.61424;
	materialDiffuse[1] = 0.04136;
	materialDiffuse[2] = 0.04136;
	materialDiffuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);

	materialSpecular[0] = 0.727811;
	materialSpecular[1] = 0.626959;
	materialSpecular[2] = 0.626959;
	materialSpecular[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);

	Shininess = 0.6 * 128;
	glMaterialf(GL_FRONT, GL_SHININESS, Shininess);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(1.5f, 4.0f, 0.0f);
	gluSphere(quadric, 1.0f, 30, 30);//glusphere call internally create all normals for it

	//***** 6th sphere on 1st column, turquoise *****
	materialAmbient[0] = 0.1;
	materialAmbient[1] = 0.18725;
	materialAmbient[2] = 0.1745;
	materialAmbient[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);


	materialDiffuse[0] = 0.396;
	materialDiffuse[1] = 0.74151;
	materialDiffuse[2] = 0.69102;
	materialDiffuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);

	materialSpecular[0] = 0.297254;
	materialSpecular[1] = 0.30829;
	materialSpecular[2] = 0.306678;
	materialSpecular[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);

	Shininess = 0.1 * 128;
	glMaterialf(GL_FRONT, GL_SHININESS, Shininess);

	glLoadIdentity();
	glTranslatef(1.5f, 1.5f, 0.0f);

	glMatrixMode(GL_MODELVIEW);
	gluSphere(quadric, 1.0f, 30, 30);//glusphere call internally create all normals for it


	// ***** 1st sphere on 2nd column, brass *****
	materialAmbient[0] = 0.329412;
	materialAmbient[1] = 0.223529;
	materialAmbient[2] = 0.027451;
	materialAmbient[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);


	materialDiffuse[0] = 0.780392;
	materialDiffuse[1] = 0.568627;
	materialDiffuse[2] = 0.113725;
	materialDiffuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);

	materialSpecular[0] = 0.992157;
	materialSpecular[1] = 0.941176;
	materialSpecular[2] = 0.808743;
	materialSpecular[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);

	Shininess = 0.21794872 * 128;
	glMaterialf(GL_FRONT, GL_SHININESS, Shininess);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(7.5f, 14.0f, 0.0f);
	gluSphere(quadric, 1.0f, 30, 30);//glusphere call internally create all normals for it

	//***** 2nd sphere on 2nd column, bronze *****
	materialAmbient[0] = 0.2125;
	materialAmbient[1] = 0.1275;
	materialAmbient[2] = 0.054;
	materialAmbient[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);


	materialDiffuse[0] = 0.714;
	materialDiffuse[1] = 0.4284;
	materialDiffuse[2] = 0.18144;
	materialDiffuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);

	materialSpecular[0] = 0.393548;
	materialSpecular[1] = 0.271906;
	materialSpecular[2] = 0.166721;
	materialSpecular[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);

	Shininess = 0.2 * 128;
	glMaterialf(GL_FRONT, GL_SHININESS, Shininess);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(7.5f, 11.5f, 0.0f);
	gluSphere(quadric, 1.0f, 30, 30);//glusphere call internally create all normals for it



	//***** 3rd sphere on 2nd column, chrome *****
	materialAmbient[0] = 0.25;
	materialAmbient[1] = 0.25;
	materialAmbient[2] = 0.25;
	materialAmbient[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);


	materialDiffuse[0] = 0.4;
	materialDiffuse[1] = 0.4;
	materialDiffuse[2] = 0.4;
	materialDiffuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);

	materialSpecular[0] = 0.774597;
	materialSpecular[1] = 0.774597;
	materialSpecular[2] = 0.774597;
	materialSpecular[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);

	Shininess = 0.6 * 128;
	glMaterialf(GL_FRONT, GL_SHININESS, Shininess);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(7.5f, 9.0f, 0.0f);
	gluSphere(quadric, 1.0f, 30, 30);//glusphere call internally create all normals for it

	//***** 4th sphere on 2nd column, copper *****
	materialAmbient[0] = 0.19125;
	materialAmbient[1] = 0.0735;
	materialAmbient[2] = 0.0225;
	materialAmbient[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);


	materialDiffuse[0] = 0.7038;
	materialDiffuse[1] = 0.27048;
	materialDiffuse[2] = 0.0828;
	materialDiffuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);

	materialSpecular[0] = 0.256777;
	materialSpecular[1] = 0.137622;
	materialSpecular[2] = 0.086014;
	materialSpecular[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);

	Shininess = 0.1 * 128;
	glMaterialf(GL_FRONT, GL_SHININESS, Shininess);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(7.5f, 6.5f, 0.0f);
	gluSphere(quadric, 1.0f, 30, 30);//glusphere call internally create all normals for it

	//***** 5th sphere on 2nd column, gold *****
	
	materialAmbient[0] = 0.24725;
	materialAmbient[1] = 0.1995;
	materialAmbient[2] = 0.0745;
	materialAmbient[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);


	materialDiffuse[0] = 0.75164;
	materialDiffuse[1] = 0.60648;
	materialDiffuse[2] = 0.22648;
	materialDiffuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);

	materialSpecular[0] = 0.628281;
	materialSpecular[1] = 0.555802;
	materialSpecular[2] = 0.366065;
	materialSpecular[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);

	Shininess = 0.4 * 128;
	glMaterialf(GL_FRONT, GL_SHININESS, Shininess);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(7.5f, 4.0f, 0.0f);
	gluSphere(quadric, 1.0f, 30, 30);//glusphere call internally create all normals for it

	//***** 6th sphere on 2nd column, silver *****
	materialAmbient[0] = 0.19225;
	materialAmbient[1] = 0.19225;
	materialAmbient[2] = 0.19225;
	materialAmbient[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);


	materialDiffuse[0] = 0.50754;
	materialDiffuse[1] = 0.50574;
	materialDiffuse[2] = 0.50574;
	materialDiffuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);

	materialSpecular[0] = 0.508273;
	materialSpecular[1] = 0.508273;
	materialSpecular[2] = 0.508273;
	materialSpecular[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);

	Shininess = 0.4 * 128;
	glMaterialf(GL_FRONT, GL_SHININESS, Shininess);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(7.5f, 1.5f, 0.0f);
	gluSphere(quadric, 1.0f, 30, 30);//glusphere call internally create all normals for it

	// ***** 1st sphere on 3rd column, black *****
	materialAmbient[0] = 0.0;
	materialAmbient[1] = 0.0;
	materialAmbient[2] = 0.0;
	materialAmbient[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);


	materialDiffuse[0] = 0.01;
	materialDiffuse[1] = 0.01;
	materialDiffuse[2] = 0.01;
	materialDiffuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);

	materialSpecular[0] = 0.50;
	materialSpecular[1] = 0.50;
	materialSpecular[2] = 0.50;
	materialSpecular[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);

	Shininess = 0.25 * 128;
	glMaterialf(GL_FRONT, GL_SHININESS, Shininess);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(13.5f, 14.0f, 0.0f);
	gluSphere(quadric, 1.0f, 30, 30);//glusphere call internally create all normals for it

	// ***** 2nd sphere on 3rd column, cyan *****
	materialAmbient[0] = 0.0;
	materialAmbient[1] = 0.1;
	materialAmbient[2] = 0.06;
	materialAmbient[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);


	materialDiffuse[0] = 0.0;
	materialDiffuse[1] = 0.50980392;
	materialDiffuse[2] = 0.50980392;
	materialDiffuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);

	materialSpecular[0] = 0.50196078;
	materialSpecular[1] = 0.50196078;
	materialSpecular[2] = 0.50196078;
	materialSpecular[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);

	Shininess = 0.25 * 128;
	glMaterialf(GL_FRONT, GL_SHININESS, Shininess);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(13.5f, 11.5f, 0.0f);
	gluSphere(quadric, 1.0f, 30, 30);//glusphere call internally create all normals for it

	// ***** 3rd sphere on 2nd column, green *****
	materialAmbient[0] = 0.0;
	materialAmbient[1] = 0.0;
	materialAmbient[2] = 0.0;
	materialAmbient[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);


	materialDiffuse[0] = 0.1;
	materialDiffuse[1] = 0.35;
	materialDiffuse[2] = 0.1;
	materialDiffuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);

	materialSpecular[0] = 0.45;
	materialSpecular[1] = 0.55;
	materialSpecular[2] = 0.45;
	materialSpecular[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);

	Shininess = 0.25 * 128;
	glMaterialf(GL_FRONT, GL_SHININESS, Shininess);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(13.5f, 9.0f, 0.0f);
	gluSphere(quadric, 1.0f, 30, 30);//glusphere call internally create all normals for it


	// ***** 4th sphere on 3rd column, red *****
	materialAmbient[0] = 0.0;
	materialAmbient[1] = 0.0;
	materialAmbient[2] = 0.0;
	materialAmbient[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);


	materialDiffuse[0] = 0.5;
	materialDiffuse[1] = 0.0;
	materialDiffuse[2] = 0.0;
	materialDiffuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);

	materialSpecular[0] = 0.7;
	materialSpecular[1] = 0.6;
	materialSpecular[2] = 0.6;
	materialSpecular[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);

	Shininess = 0.25 * 128;
	glMaterialf(GL_FRONT, GL_SHININESS, Shininess);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(13.5f, 6.5f, 0.0f);
	gluSphere(quadric, 1.0f, 30, 30);//glusphere call internally create all normals for it

	// ***** 5th sphere on 3rd column, white *****
	materialAmbient[0] = 0.0;
	materialAmbient[1] = 0.0;
	materialAmbient[2] = 0.0;
	materialAmbient[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);


	materialDiffuse[0] = 0.55;
	materialDiffuse[1] = 0.55;
	materialDiffuse[2] = 0.55;
	materialDiffuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);

	materialSpecular[0] = 0.70;
	materialSpecular[1] = 0.70;
	materialSpecular[2] = 0.70;
	materialSpecular[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);

	Shininess = 0.25 * 128;
	glMaterialf(GL_FRONT, GL_SHININESS, Shininess);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(13.5f, 4.0f, 0.0f);
	gluSphere(quadric, 1.0f, 30, 30);//glusphere call internally create all normals for it

	//6th sphere on 3rd column, yellow plastic
	materialAmbient[0] = 0.0;
	materialAmbient[1] = 0.0;
	materialAmbient[2] = 0.0;
	materialAmbient[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);


	materialDiffuse[0] = 0.5;
	materialDiffuse[1] = 0.5;
	materialDiffuse[2] = 0.0;
	materialDiffuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);

	materialSpecular[0] = 0.60;
	materialSpecular[1] = 0.60;
	materialSpecular[2] = 0.50;
	materialSpecular[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);

	Shininess = 0.25 * 128;
	glMaterialf(GL_FRONT, GL_SHININESS, Shininess);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(13.5f, 1.5f, 0.0f);
	gluSphere(quadric, 1.0f, 30, 30);//glusphere call internally create all normals for it

	//glPolygonMode(GL_FRONT_AND_BACK,GL_FILL);

	// ***** 1st sphere on 4th column, black *****
	materialAmbient[0] = 0.02;
	materialAmbient[1] = 0.02;
	materialAmbient[2] = 0.02;
	materialAmbient[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);


	materialDiffuse[0] = 0.01;
	materialDiffuse[1] = 0.01;
	materialDiffuse[2] = 0.01;
	materialDiffuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);

	materialSpecular[0] = 0.4;
	materialSpecular[1] = 0.4;
	materialSpecular[2] = 0.4;
	materialSpecular[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);

	Shininess = 0.078125 * 128;
	glMaterialf(GL_FRONT, GL_SHININESS, Shininess);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(19.5f, 14.0f, 0.0f);
	gluSphere(quadric, 1.0f, 30, 30);//glusphere call internally create all normals for it
	
	// ***** 2nd sphere on 4th column, cyan *****
	materialAmbient[0] = 0.0;
	materialAmbient[1] = 0.05;
	materialAmbient[2] = 0.05;
	materialAmbient[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);


	materialDiffuse[0] = 0.4;
	materialDiffuse[1] = 0.5;
	materialDiffuse[2] = 0.5;
	materialDiffuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);

	materialSpecular[0] = 0.04;
	materialSpecular[1] = 0.7;
	materialSpecular[2] = 0.7;
	materialSpecular[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);

	Shininess = 0.078125 * 128;
	glMaterialf(GL_FRONT, GL_SHININESS, Shininess);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(19.5f, 11.5f, 0.0f);
	gluSphere(quadric, 1.0f, 30, 30);//glusphere call internally create all normals for it

	// ***** 3rd sphere on 4th column, green *****
	materialAmbient[0] = 0.0;
	materialAmbient[1] = 0.05;
	materialAmbient[2] = 0.0;
	materialAmbient[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);


	materialDiffuse[0] = 0.4;
	materialDiffuse[1] = 0.5;
	materialDiffuse[2] = 0.4;
	materialDiffuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);

	materialSpecular[0] = 0.04;
	materialSpecular[1] = 0.7;
	materialSpecular[2] = 0.04;
	materialSpecular[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);

	Shininess = 0.078125 * 128;
	glMaterialf(GL_FRONT, GL_SHININESS, Shininess);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(19.5f, 9.0f, 0.0f);
	gluSphere(quadric, 1.0f, 30, 30);//glusphere call internally create all normals for it

	// ***** 4th sphere on 4th column, red *****
	materialAmbient[0] = 0.05;
	materialAmbient[1] = 0.0;
	materialAmbient[2] = 0.0;
	materialAmbient[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);


	materialDiffuse[0] = 0.5;
	materialDiffuse[1] = 0.4;
	materialDiffuse[2] = 0.4;
	materialDiffuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);

	materialSpecular[0] = 0.7;
	materialSpecular[1] = 0.04;
	materialSpecular[2] = 0.04;
	materialSpecular[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);

	Shininess = 0.078125 * 128;
	glMaterialf(GL_FRONT, GL_SHININESS, Shininess);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(19.5f, 6.5f, 0.0f);
	gluSphere(quadric, 1.0f, 30, 30);//glusphere call internally create all normals for it

	// ***** 5th sphere on 4th column, white *****
	materialAmbient[0] = 0.05;
	materialAmbient[1] = 0.05;
	materialAmbient[2] = 0.05;
	materialAmbient[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);


	materialDiffuse[0] = 0.5;
	materialDiffuse[1] = 0.5;
	materialDiffuse[2] = 0.5;
	materialDiffuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);

	materialSpecular[0] = 0.7;
	materialSpecular[1] = 0.7;
	materialSpecular[2] = 0.7;
	materialSpecular[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);

	Shininess = 0.078125 * 128;
	glMaterialf(GL_FRONT, GL_SHININESS, Shininess);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(19.5f, 4.0f, 0.0f);
	gluSphere(quadric, 1.0f, 30, 30);//glusphere call internally create all normals for it

	//  ***** 6th sphere on 4th column, yellow rubber *****
	materialAmbient[0] = 0.05;
	materialAmbient[1] = 0.05;
	materialAmbient[2] = 0.0;
	materialAmbient[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);


	materialDiffuse[0] = 0.5;
	materialDiffuse[1] = 0.5;
	materialDiffuse[2] = 0.4;
	materialDiffuse[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);

	materialSpecular[0] = 0.7;
	materialSpecular[1] = 0.7;
	materialSpecular[2] = 0.04;
	materialSpecular[3] = 1.0f;
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);

	Shininess = 0.078125 * 128;
	glMaterialf(GL_FRONT, GL_SHININESS, Shininess);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	glTranslatef(19.5f, 1.5f, 0.0f);
	gluSphere(quadric, 1.0f, 30, 30);//glusphere call internally create all normals for it

    glXSwapBuffers(display,window);
}
void update (void)
{

		//code
	if (keyPressed == 1)
	{
		angleForXRotation = angleForXRotation + 1.0f;

		if (angleForXRotation > 360.0f)
		{
			angleForXRotation = angleForXRotation - 360.0f;
		}
	}
	if (keyPressed == 2)
	{
		angleForYRotation = angleForYRotation + 1.0f;
		
		if (angleForYRotation > 360.0f)
		{
			angleForYRotation = angleForYRotation - 360.0f;
		}
	}
	if (keyPressed == 3)
	{
		angleForZRotation = angleForZRotation + 1.0f;

		if (angleForZRotation > 360.0f)
		{
			angleForZRotation = angleForZRotation - 360.0f;
		}

	}
}
//define function
void uninitialise(void)
{
	//code

	GLXContext currentGLXContext = NULL;

    //code
    if(visualInfo)
    {
        free(visualInfo);
        visualInfo =  NULL;
    }
    //uncurrent the context
    currentGLXContext = glXGetCurrentContext();
    if(currentGLXContext!=NULL && currentGLXContext == glxContext)
    {
        glXMakeCurrent(display,0,0);
    }

    if(glxContext)
    {
        glXDestroyContext(display,glxContext);
        glxContext = NULL;
    }

	if (window)
	{
		XDestroyWindow(display, window);

	}

	if (colormap)
	{
		XFreeColormap(display,colormap);
	}

	if (quadric)
	{
		gluDeleteQuadric(quadric);
		quadric = NULL;
	}

	if (display)
	{
		XCloseDisplay(display);
		display = NULL;
	}


}
