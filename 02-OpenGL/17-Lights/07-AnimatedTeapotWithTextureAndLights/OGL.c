//Standard Header File
#include <stdio.h> // for printf
#include <stdlib.h> // for exit
#include <memory.h> //memset

#include <SOIL/SOIL.h>
//x11 header files
#include <X11/Xlib.h> //forall x window API
#include <X11/Xutil.h>//for x visual info and related API
#include <X11/XKBlib.h>

#include <GL/gl.h>
#include <GL/glx.h>
#include <GL/glu.h>

#include"OGL.h" 
#include"Model.h"
//macros
#define WIN_WIDTH 800
#define WIN_HEIGHT 600

//global variable declaration
FILE *gpFile = NULL;
Display *display = NULL;
Colormap colormap;
Window window;
XVisualInfo *visualInfo;

Bool bFullScreen=False;

Bool bActiveWindow = False;
GLXContext glxContext =NULL;

GLuint texture_model = 0;

//lights variables
Bool bLight = False;
Bool bTexture = False;

GLfloat lightAmbient[] = { 0.0f,0.0f,0.0f,1.0f };//gray ambient light
GLfloat lightDiffused[] = {1.0f,1.0f,1.0f,1.0f}; // white diffused light
GLfloat lightSpecular[] = {1.0f,1.0f,1.0f,1.0f};
GLfloat lightPosition[] = {100.0f,100.0f,100.0f,1.0f};

GLfloat materialAmbient[] = { 0.0f,0.0f,0.0f, 1.0f};
GLfloat materialDiffused[] = { 1.0f,1.0f,1.0f, 1.0f };
GLfloat materialSpecular[] = { 1.0f,1.0f,1.0f, 1.0f };
GLfloat materialShininess[] = { 128.0f};

//For Animation
Bool bAnimate = False;
GLfloat Angle = 0.0f;

//GLUquadric* quadric = NULL;
//main
int main(void)
{
	//local function declarations
	void uninitialise(void);
	void toggleFullScreen(void);
	void draw(void);
	void update(void);
	int initialize(void);
	void resize(int,int);

	//local variable declarations
	int defaultScreen;
	int defaultDepth;	
	Status status;
	XSetWindowAttributes windowAttributes;
	int styleMask;
	Atom windowManagerDelete;
	XEvent event;
	KeySym keySym;
	char keys[26];

	int screenWidth;
	int screenHeight;


	int frameBufferAttributes[]={GLX_DOUBLEBUFFER,True,
                                 GLX_RGBA,
                                 GLX_RED_SIZE,8,
                                 GLX_GREEN_SIZE,8,
                                 GLX_BLUE_SIZE,8,
                                 GLX_ALPHA_SIZE,8,
                                 None};
	
 Bool bDONE= False;
 int iResult = 0;

	
	//code
	//Step 1.open connection with server 
	display = XOpenDisplay(NULL); //1 api
	if (display == NULL)
	{
		printf("XOpenDisplay() Failed\n");
		uninitialise();
		exit(1); //failure exit 
	}
											
	// Step 2.get default screen from above display
	defaultScreen = XDefaultScreen(display); //2 api

	//step3  get default depth from above 2
	visualInfo = glXChooseVisual(display,defaultScreen,frameBufferAttributes);
    if(visualInfo == NULL)
    {
        printf("glXChooseVisual failed\n");
        uninitialise();
        exit(1);
    }
//step 4.set window attribute
	memset((void*)&windowAttributes,0,sizeof(XSetWindowAttributes));
	windowAttributes.border_pixel = 0;
	windowAttributes.background_pixel = XBlackPixel(display, visualInfo->screen);//5API
	windowAttributes.background_pixmap = 0;
	windowAttributes.colormap = XCreateColormap(display,XRootWindow(display, visualInfo->screen), visualInfo->visual,AllocNone);
	
	//step 5.assign this color map to global color map 
	colormap = windowAttributes.colormap;

	//set the stylemask 
	styleMask = CWBorderPixel|CWBackPixel|CWColormap|CWEventMask;

	//now finally create window
	//window = XCreateWindow(display, XRootWindow(display, (visualInfo.screen),0,0,WIN_WIDTH,WIN_HEIGHT,0,visualInfo.depth,InputOutput, visualInfo.visual, styleMask,&windowAttribute));
	
	// 8. Create Window
	window = XCreateWindow(display,
						XRootWindow(display, visualInfo->screen),
						0,
						0,
						WIN_WIDTH,
						WIN_HEIGHT,
						0,
						visualInfo->depth,
						InputOutput,
						visualInfo->visual,
						styleMask,
						&windowAttributes);
                
	if(!window)
	{
		printf("xCreateWindow() Failed\n");
		uninitialise();
		exit(1); //failure exit 
	}
	//specify to which event this window  to respond 

	XSelectInput(display,window,ExposureMask|VisibilityChangeMask|StructureNotifyMask|KeyPressMask|ButtonPressMask|PointerMotionMask|FocusChangeMask);
	
	//specify window manger delete atom 
	windowManagerDelete = XInternAtom(display,"WM_DELETE_WINDOW",True);

	//Add / Set above atom as protocol for window manager
	XSetWMProtocols(display,window,&windowManagerDelete,1);

	//give caption to window
	XStoreName(display,window,"PANKAJ MUKUND KALE");

	//show/map the  window
	XMapWindow(display,window);

//center the window.
screenWidth = XWidthOfScreen(XScreenOfDisplay(display,visualInfo->screen));
screenHeight = XHeightOfScreen(XScreenOfDisplay(display,visualInfo->screen));

XMoveWindow(display,window,(screenWidth-WIN_WIDTH)/2,(screenHeight-WIN_HEIGHT)/2);
//opengl intialization
    iResult = initialize();
    if (iResult != 0)
	{
		printf("initialize() failed");
		exit(1);
	}



	//Event Loop
    while(bDONE==False)
    {   while(XPending(display))
		{

	
        memset((void*)&event, 0, sizeof(XEvent));
        XNextEvent(display,&event);
        switch (event.type)
        {

			case FocusIn:
                bActiveWindow = True;
                break;

            case FocusOut:
                bActiveWindow = False;
                break;

            case ConfigureNotify:
                resize(event.xconfigure.width,event.xconfigure.height);
                break;


        case KeyPress:
             keySym=XkbKeycodeToKeysym(display,event.xkey.keycode,0,0);//WM_KEYDOWN
             switch (keySym)
             {
             case XK_Escape:
                        uninitialise();
                        exit(0);
             break;
             
             default:
                break;
             }

             XLookupString(&event.xkey,keys,sizeof(keys),NULL,NULL);//WM_CHAR
             switch (keys[0])
             {
             case 'F':
             case 'f':
                        if(bFullScreen==False)
                        {
							
                            toggleFullScreen();
                            bFullScreen=True;
                        }
                        else
                        {
                            toggleFullScreen();
                            bFullScreen=False;
                        }
                break;
				
				case 'L':
			case'l':
				if (bLight == False)
				{
					
					bLight = True;
				}
				else
				{
					
					bLight = False;
				}
				break;

			case 'T':
			case't':
				if (bTexture == False)
				{

					bTexture = True;
				}
				else
				{

					bTexture = False;
				}
				break;

			case 'A':
			case'a':
				if (bAnimate == False)
				{

					bAnimate = True;
				}
				else
				{

					bAnimate = False;
				}
				break;

            
             	default:
                break;
             }
        break;
        
        case 33:
                uninitialise();
                exit(0);
        break;
        
        	default:
            break;

        }

		}
		if(bActiveWindow == True)
		{
			draw();
			update();

		}

    }

	
	uninitialise();
	return (0);
	
}



void toggleFullScreen(void)
{

	//local variable decalrations
	Atom windowManagerStateNormal;	
	Atom windowManagerStateFullScreen;
	XEvent event;

	//code

	windowManagerStateNormal=XInternAtom(display,"_NET_WM_STATE",False);
 	
	windowManagerStateFullScreen=XInternAtom(display,"_NET_WM_STATE_FULLSCREEN",False);
	
	//memset the e vent structure and delete with the aboove two item


	memset((void*)&event,0,sizeof(XEvent));

	event.type=ClientMessage;
	event.xclient.window = window;
	event.xclient.message_type=windowManagerStateNormal;
	event.xclient.format = 32;
	event.xclient.data.l[0] = bFullScreen ? 0:1;
	event.xclient.data.l[1]= windowManagerStateFullScreen;

	//send the event
	
	XSendEvent(display,XRootWindow(display,visualInfo->screen),False,SubstructureNotifyMask,&event);

}

int initialize(void)
{
	Bool bresult=False;
    void resize(int,int);
    Bool loadGLTexture(GLuint* texture, const char* fileName)
    //code
    //create opengl context
    glxContext = glXCreateContext(display,visualInfo,NULL,True);
    if(glxContext == NULL)
    {
        printf("In initialize glxCreateContext fialed\n");
        return(-1);
    }
    //make this context as current context
    if(glXMakeCurrent(display,window,glxContext) == False)
    {
        printf("glxMakeCurrent failed\n");
        return(-2);
    }

//usual opengl code
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);    //opengl starts here
    
//light related initialisation

	bresult = loadGLTexture(&texture_model, "./marble.bmp");
	if (bresult == False)
	{
		fprintf(gpFile, "loading of model texture failed\n");

		return -7;
	}

	//light related initialisation

	glLightfv(GL_LIGHT0,GL_AMBIENT,lightAmbient);// v>array(vector) 
	glLightfv(GL_LIGHT0,GL_DIFFUSE,lightDiffused);
	glLightfv(GL_LIGHT0,GL_SPECULAR,lightSpecular);
	glLightfv(GL_LIGHT0,GL_POSITION,lightPosition);
	
	//material property
	glMaterialfv(GL_FRONT,GL_AMBIENT,materialAmbient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffused);
	glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);
	glMaterialfv(GL_FRONT, GL_SHININESS, materialShininess);


	//initialise quadric
	//quadric = gluNewQuadric();
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

	glEnable(GL_LIGHT0);




    //warmup resize
    resize(WIN_WIDTH,WIN_HEIGHT);
    return(0);
}


Bool loadGLTexture1(GLuint* texture,const char *path)
{
	//local variable declarations
int width;
int height;
unsigned char *imageData = NULL;
GLuint texture;

	//HBITMAP hBitmap = NULL;
	//BITMAP bmp;

	//fifth step: load the image

	//hBitmap = (HBITMAP)LoadImage(GetModuleHandle(NULL), imageresourceID, IMAGE_BITMAP, 0, 0, LR_CREATEDIBSECTION);
	
imageData = SOIL_load_image(path,&width,&height,NULL,SOIL_LOAD_RGB);
	//sixth step :get image data
	//GetObject(hBitmap, sizeof(BITMAP), &bmp);
	if (imageData == NULL)
{
	fprintf(gpFile, "loading of image failed\n");
}

	//seventh step : create opengl textures.
	glGenTextures(1, &texture);

	//eight step: Bind to the genreated textur
	glBindTexture(GL_TEXTURE_2D, texture);

	//ninths step:
	glPixelStorei(GL_UNPACK_ALIGNMENT, 4);

	//tenth step: set the texture parameter
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	//ELEVENTH STEP
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);

	//CREATE MULTIPLE MIPMAP IMAGES
	gluBuild2DMipmaps(GL_TEXTURE_2D, 3, width, height, GL_RGB, (void *)imageData);
	SOIL_free_image_data(imageData);
	return (texture);
	//glBindTexture(GL_TEXTURE_2D, 0);

	//delete image  resource
	//DeleteObject(hBitmap);

	//hBitmap = NULL;


	//return TRUE;
}


Bool loadGLTexture(GLuint* texture, const char* fileName)
{
	// local variable declarations
	int width, height;
	unsigned char* imageData = NULL;

	// Load the image
	imageData = SOIL_load_image(fileName, &width, &height, NULL, SOIL_LOAD_RGB);
	if(imageData == NULL)
	{
		return False;
	}

	// Create OpenGL texture object
	glGenTextures(1, texture);

	// Bind to the generated texture
	glBindTexture(GL_TEXTURE_2D, *texture);

	// Decide image pixel alignment and unpacking
	glPixelStorei(GL_UNPACK_ALIGNMENT, 4);

	// Set texture parameters
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);

	// Accordingly use image data and texture parameters to 
	// create multiple such images which will map with 
	// objects in the scene to render
	gluBuild2DMipmaps(GL_TEXTURE_2D, 3, width, height, 
		GL_RGB, GL_UNSIGNED_BYTE, (void*)imageData);

	// Unbind with OpenGL texture object
	glBindTexture(GL_TEXTURE_2D, 0);

	// Delete image object
	SOIL_free_image_data(imageData);

	return True;
}

void resize(int width,int height)
{
    //code
    if(height <= 0)
    {
	    height = 1;
	}

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	glViewport(0, 0, (GLsizei)width, (GLsizei)height);
	gluPerspective(45.0f, (GLfloat)width / (GLfloat)height,0.1f,100.0f);
}
void draw(void)
{
    //code
   //local variable declarations
	int i, j;
	int vi, ni, ti;
	glClear(GL_COLOR_BUFFER_BIT |GL_DEPTH_BUFFER_BIT);//blue colour bhint zhali ithe
	glMatrixMode(GL_MODELVIEW); // model view matri tayar hote internall ithe
	glLoadIdentity(); // identity matrix tayar hote inrernally ithe
	glTranslatef(0.0f,0.0f,-1.5f);
	glRotatef(Angle,0.0f,1.0f,0.0f);

	
	//Toggle Lighting
	if (bLight == True)
	{
		glEnable(GL_LIGHTING);
	}

	else
	{
		glDisable(GL_LIGHTING);
	}
	

	//Toggle Texture
	if (bTexture == True)
	{
		glEnable(GL_TEXTURE_2D);
		glBindTexture(GL_TEXTURE_2D, texture_model);
	}
	else
	{
		glDisable(GL_TEXTURE_2D);
		glBindTexture(GL_TEXTURE_2D, 0);
	}
	glBegin(GL_TRIANGLES);
	for (i = 0; i < sizeof(face_indicies) / sizeof(face_indicies[0]); i++)
	{
		for (j = 0; j < 3; j++)//vertex, normal,texture

		{
			vi = face_indicies[i][j];
			ni = face_indicies[i][j + 3];
			ti = face_indicies[i][j + 6];

			glNormal3f(normals[ni][0], normals[ni][1], normals[ni][2]);
			glTexCoord2f(textures[ti][0], textures[ti][1]);
			glVertex3f(vertices[vi][0], vertices[vi][1], vertices[vi][2]);
		}

	}
	glEnd();



    glXSwapBuffers(display,window);
}
void update (void)
{
	//code
	Angle = Angle + 1.0f;
	if (Angle > 360.0f)
	{
		Angle = Angle - 360.0f;
	}
}
//define function
void uninitialise(void)
{
	//code

	GLXContext currentGLXContext = NULL;

    //code
    if(visualInfo)
    {
        free(visualInfo);
        visualInfo =  NULL;
    }
    //uncurrent the context
    currentGLXContext = glXGetCurrentContext();
    if(currentGLXContext!=NULL && currentGLXContext == glxContext)
    {
        glXMakeCurrent(display,0,0);
    }

    if(glxContext)
    {
        glXDestroyContext(display,glxContext);
        glxContext = NULL;
    }

	if (window)
	{
		XDestroyWindow(display, window);

	}

	if (colormap)
	{
		XFreeColormap(display,colormap);
	}

	if (texture_model)
	{
		glDeleteTextures(1, &texture_model);
		texture_model = 0;
	}

	if (display)
	{
		XCloseDisplay(display);
		display = NULL;
	}
	if (gpFile)
	{
		fprintf(gpFile, "Programme Ended Successfully.\n");
		fclose(gpFile);
		gpFile = NULL;
	}

}
